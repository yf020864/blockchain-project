﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Transaction
    {
        public String hash;
        public String signature;
        public String senderAddress;
        public String recipientAddress;
        public DateTime timestamp;
        public double amount;
        public double fee;

        public Transaction(String sender, String recipent, double amt, double feee, String privKey)
        {
            timestamp = DateTime.Now;
            senderAddress = sender;
            recipientAddress = recipent;
            amount = amt;
            fee = feee;

            hash = CreateHash();
            signature = Wallet.Wallet.CreateSignature(senderAddress, privKey, hash);
        }

        public String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();

            String input = timestamp + senderAddress + recipientAddress + amount + fee;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            String hash = string.Empty;

            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }

        public String asString()
        {
            string x = "\nTransaction Hash: "+hash+"\nDigital Signature: "+signature+"\nTimestamp: "+timestamp+"\nTransferred: "+amount+" Coin\nFees: "+fee+"\nSender Address: "+senderAddress+"\nRecipient Address:"+recipientAddress+"\n";
            return x;
        }
    }
}
