﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    internal class Blockchain
    {
        public List<Block> blocks = new List<Block>();
        
        int transactionCap = 5;
        public List<Transaction> transactionPool = new List<Transaction>();


        public Blockchain()
        {
            blocks.Add(new Block());
        }

        public String blockAsString(int index)
        {
            return blocks[index].asString();
        }
        public Block getLastBlock()
        {
            return blocks[blocks.Count - 1];
        }

        public List<Transaction> getPool()
        {
            List<Transaction> chosenTransactions = new List<Transaction>();
            chosenTransactions = transactionPool.Take(transactionCap - 1).ToList();

            transactionPool = transactionPool.Except(chosenTransactions).ToList();

            return chosenTransactions;
        }

        public List<Transaction> getGreedy()
        {
            List<Transaction> chosen = new List<Transaction>();

            for (int i = 0; i < transactionCap; i++)
            {
                if (transactionPool.Count == 0)
                {
                    break;
                }
                double largestFeeVal = -1;
                Transaction largestFeeTrans = null;

                foreach (Transaction transaction in transactionPool)
                {
                    if (transaction.fee > largestFeeVal)
                    {
                        largestFeeVal = transaction.fee;
                        largestFeeTrans = transaction;
                    }
                }
                chosen.Add(largestFeeTrans);
                transactionPool.Remove(largestFeeTrans);

                
            }

            return chosen;
        }

        public List<Transaction> getAltruistic()
        {
            List<Transaction> chosen = new List<Transaction>();

            for (int i = 0; i < transactionCap; i++)
            {
                if (transactionPool.Count == 0)
                {
                    break;
                }

                DateTime oldestAge = DateTime.Now;
                Transaction oldestTrans = null;

                foreach (Transaction transaction in transactionPool)
                {
                    if (DateTime.Compare(transaction.timestamp, oldestAge) < 0)
                    {
                        oldestAge = transaction.timestamp;
                        oldestTrans = transaction;
                    }
                }
                chosen.Add(oldestTrans);
                transactionPool.Remove(oldestTrans);

                
            }

            return chosen;
        }
        public List<Transaction> getRelevantTransactions(String address)
        {
            List<Transaction> transLog = new List<Transaction>();
            
            foreach (Block block in blocks)
            {
                foreach(Transaction transaction in block.transactionList)
                {
                    if (transaction.recipientAddress == address || transaction.senderAddress == address)
                    {
                        transLog.Add(transaction);
                    }

                }
            }

            return transLog;
        }

        public double getBalance(String address)
        {
            double balance = 0;
            List<Transaction> relevant = getRelevantTransactions(address);
            foreach (Transaction transaction in relevant)
            {
                if (transaction.senderAddress.Equals(address))
                {
                    balance -= transaction.amount;
                }

                if (transaction.recipientAddress.Equals(address))
                {
                    balance += transaction.amount;
                }
            }
            return balance;
        }
        public bool validateHash(Block block)
        {
            String rehash = block.CreateHash();
            return rehash.Equals(block.getHash());
        }
        public bool validateMerkleRoot(Block block)
        {
            String reMerkle = Block.MerkleRoot(block.transactionList);
            return reMerkle.Equals(block.merkleRoot);
        }
        public String asString()
        {
            string x = "";
            foreach (Block block in blocks)
            {
                x = x + block.asString() +"\n";
            }
            return x;
        }

    }
}
