﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    
    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;

        public BlockchainApp()
        {
            InitializeComponent();
            blockchain = new Blockchain();
            richTextBox1.Text = "New blockchain initialised";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.blockAsString(Convert.ToInt32(textBox1.Text));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string validity = "in";
            if (Wallet.Wallet.ValidatePrivateKey(textBox2.Text, textBox3.Text) == true)
            {
                validity = "";
            }

            richTextBox1.Text = "Keys are "+validity+"valid";
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Wallet.Wallet wallet = new Wallet.Wallet(out string privateKey);

            textBox2.Text = privateKey;
            textBox3.Text = wallet.publicID;

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (blockchain.getBalance(textBox3.Text) >= Convert.ToDouble(textBox4.Text))
            {
                Transaction transaction = new Transaction(textBox3.Text, textBox6.Text, Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text), textBox2.Text);
                if (Wallet.Wallet.ValidateSignature(textBox3.Text, transaction.hash, transaction.signature))
                {
                    blockchain.transactionPool.Add(transaction);
                    richTextBox1.Text = transaction.asString();
                }
                else
                {
                    richTextBox1.Text = "invalid signature";
                }
            }
            else
            {
                richTextBox1.Text = "Insufficient Funds";
            }
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPool();

            DateTime start = DateTime.Now;
            Block newBlock = new Block(blockchain.getLastBlock(),  transactions, textBox3.Text, false);
            DateTime end = DateTime.Now;
            TimeSpan total = end - start;
            System.Diagnostics.Debug.WriteLine("Time: " + total);

            blockchain.blocks.Add(newBlock);

            richTextBox1.Text = newBlock.asString() + "\nTHREADS: 1\n TIME: " + total;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.asString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (blockchain.blocks.Count == 1)
            {

                if (!blockchain.validateMerkleRoot(blockchain.blocks[0]) || !blockchain.validateHash(blockchain.blocks[0]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;
                }
            }
            if (blockchain.blocks.Count > 1)
            {
                for (int i=1; i<blockchain.blocks.Count -1; i++)
                {
                    if (blockchain.blocks[i].getPrevHash() != blockchain.blocks[i - 1].getHash() || !blockchain.validateMerkleRoot(blockchain.blocks[i]) || !blockchain.validateHash(blockchain.blocks[i]))
                    {
                        richTextBox1.Text = "Blockchain is invalid";
                        return;
                    }
                }
            }
            richTextBox1.Text = "Blockchain is valid";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            double balance = blockchain.getBalance(textBox3.Text);
            string log = "";

            List<Transaction> relevant = blockchain.getRelevantTransactions(textBox3.Text);
            foreach (Transaction transaction in relevant)
            {
                log += transaction.asString();
            }
            richTextBox1.Text = "Address: "+ textBox3.Text+
                "\nBalance: " + balance+
                "\n\n" + log;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPool();
            Block newBlock = new Block(blockchain.getLastBlock(), transactions, textBox3.Text, false);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPool();

            DateTime start = DateTime.Now;
            Block newBlock = new Block(blockchain.getLastBlock(), transactions, textBox3.Text, true);
            DateTime end = DateTime.Now;
            TimeSpan total = end - start;
            System.Diagnostics.Debug.WriteLine("Time: " + total);

            blockchain.blocks.Add(newBlock);

            richTextBox1.Text = newBlock.asString() + "\nTHREADS: " + newBlock.threads + "\n TIME: " + total;
            

        }

        private void button10_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getGreedy();

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, textBox3.Text, false);

            blockchain.blocks.Add(newBlock);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getAltruistic();

            Block newBlock = new Block(blockchain.getLastBlock(), transactions, textBox3.Text, false);

            blockchain.blocks.Add(newBlock);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            String result = "\n";
            foreach (Transaction transaction in blockchain.getPool())
            {
                result += transaction.asString() + "\n";
            }
            richTextBox1.Text = result;

        }
    }
}
