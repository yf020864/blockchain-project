﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    internal class Block
    {
        private DateTime timestamp;
        public int index;
        public string hash;
        private string prevHash;

        public List<Transaction> transactionList;

        private long nonce;
        private int difficulty = 4;
        private double reward = 5;
        private string minerAddress;

        public string merkleRoot;

        public bool threading;
        public int threads = 2;
        public long enonce = 0;
        private CancellationToken cToken;

        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = Mine();
            prevHash = "";
        }

        public Block(Block lastBlock, List<Transaction> transactions, String miner, bool thread)
        {
            timestamp = DateTime.Now;
            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;
            //prevHash = "tampered";
            minerAddress = miner;
            threading = thread;

            transactions.Add(newRewardTransaction(transactions));

            transactionList = transactions;
            merkleRoot = MerkleRoot(transactionList);

            if (threading)
            {
                hash = MineT();
            }
            else
            {
                hash = Mine();
            }
            
            //hash = "tampered";

        }

        public Transaction newRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Sum(x => x.fee);
            Transaction transaction = new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, "");
            return transaction;
        }
        public String Mine()
        {
            nonce = 0;
            string newHash = CreateHash();
            string check = new string('0',difficulty);

            while (newHash.StartsWith(check) != true)
            {
                nonce++;
                newHash = CreateHash();
            }

            return newHash;
        }

        public String MineT()
        {
            var cancel = new CancellationTokenSource();
            cToken = cancel.Token;

            String result = "";

            int tNonce = 0;
            int tID = 0;
            ThreadLocal<String> localHash = new ThreadLocal<string>(() => { return ""; });
            ThreadLocal<int> localNonce = new ThreadLocal<int>(() => { return 0; });
            Task[] tasks = new Task[threads];
            String check = new string('0', difficulty);
            
            for (int i = 0; i < threads; i++)
            {
                tasks[i] = Task.Run(() => 
                {
                    while (!cToken.IsCancellationRequested)
                    {
                        localNonce.Value++;
                        localHash.Value = CreateHash(Thread.CurrentThread.ManagedThreadId, localNonce.Value);
                        if (localHash.Value.StartsWith(check))
                        {
                            cancel.Cancel();

                            tNonce = localNonce.Value;
                            tID = Thread.CurrentThread.ManagedThreadId;
                            result = localHash.Value;
                        }
                    }
                }
                );
            }
            Task.WaitAll(tasks);

            nonce = tNonce;
            enonce = tID;
            String hash = result;
            
            return hash;

        }
        public String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();

            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + enonce.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            String hash = string.Empty;

            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;

        }
        public String CreateHash(int enonce, int nonce)
        {
            SHA256 hasher = SHA256Managed.Create();

            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + enonce.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            String hash = string.Empty;

            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;

        }

        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList();

            if (hashes.Count == 0)
            {
                return String.Empty;
            }
            if (hashes.Count == 1)
            {
                return HashCode.HashTools.CombineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1)
            {
                List<String> merkleLeaves = new List<String>();

                for (int i = 0; i < hashes.Count; i += 2)
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i]));
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i + 1]));
                    }
                }
                hashes = merkleLeaves;
            }
            return hashes[0];
        }

        public String getPrevHash()
        {
            return prevHash;
        }

        public String getHash()
        {
            return hash;
        }


        public String asString()
        {
            String x = "Block Index: " + index +
                "\nTimestamp: " + timestamp +
                "\nHash: " + hash +
                "\nPrev Hash: " + prevHash +
                "\nTransactions: " + transactionList.Count() +
                "\nNonce: " + nonce +
                "\nDifficulty: " + difficulty +
                "\nReward: " + reward +
                "\nMiner Address: " + minerAddress +
                "\n";
            
            foreach (Transaction t in transactionList)
            {
                x += "\n" + t.asString();
            }

            return x;
        }
    }
}
